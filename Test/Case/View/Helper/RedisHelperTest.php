<?php
App::uses('RedisHelper', 'Cakeredis.View/Helper');
App::uses('View', 'View');

class RedisHelperTest extends CakeTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->View = new View(null);
        $this->Redis = new RedisHelper($this->View, array(
                'namespace' => 'testcase',
                'select' => 2
            )
        );

    }

    public function tearDown()
    {
        unset($this->Redis);
        ob_flush();
    }

    public function test新着情報を管理()
    {

//        100 のユーザースコアを削除
        $this->Redis->deleteAll("100");

        //        //user_id  に 100 に ラーメンと追加。 ラーメンがかぶると上書きされるので、 time をつけておく。
        $this->Redis->addNews("100","ラーメン".time());
        $this->Redis->addNews("100","そば".time());
        $this->Redis->addNews("100","うどん");
        $this->Redis->addNews("100","パスタ");

        //  新着情報を 5 件取得する
        $res = $this->Redis->getNews("100",5);
        $this->assertEquals(4, count($res));


        ////        新着ニュースをカウント / MAXカウントこれを超える場合は 10+ のような表記にする
        $count = $this->Redis->countNews("100",2);
        $this->assertEquals("2+", $count);


//        //        削除
        $this->Redis->deleteNews("100","パスタ");
        $res = $this->Redis->getNews("100",5);
        $this->assertEquals(3, count($res));







    }

//    public function testキーを正規表現で取得()
//    {
//        $this->Redis->setData("test_hoge","hanako");
//        $this->Redis->setData("test_raumen","tarou");
//        $res = $this->Redis->getKeys("*test_*");
//
//        $this->assertContains("test_hoge", $res);
//        $this->assertContains("test_raumen",$res);
//
//
//        $this->Redis->setData("chomoranma","tarou");
//        $res = $this->Redis->getKeys("*");
//        $this->assertContains("chomoranma",$res);
//
//
//
//    }
//
//
//
//    public function testキーがあるか調べる()
//    {
//        $res = $this->Redis->setData("yourname","hanako");
//        $this->assertTrue($this->Redis->exists("yourname"));
//        $this->assertFalse($this->Redis->exists("nothing"));
//    }
//
//    public function testAnalyticsのデータを読み出せるか()
//    {
////        score が一番多いものから順に8件取り出す。
//        $res = $this->Redis->revRange("Matomater-articleCount:total:total:total",0,7,true);
//
//        $this->assertEquals(true, is_array($res));
//
//
//        $score = $this->Redis->score("Matomater-articleCount:total:total:total",1629);
//        $this->assertEquals(true, is_numeric($score));//314688
//
//        $score = $this->Redis->incr("Matomater-articleCount:total:total:total", 1629, 1);
//        $this->assertEquals(true, is_numeric($score));//314689
//
//    }
//
//
//
//    public function test文字列型を格納()
//    {
//        $res = $this->Redis->setData("yourname","hanako");
//        $res = $this->Redis->getData("yourname");
//        $this->assertEquals('hanako', $res);
//    }
//
//
//    public function test過去30日及び7日及び1日の範囲を取得()
//    {
//        $date = $this->Redis->getRangeDate();
//
//        $this->assertEquals(30, count($date['month']));
//        $this->assertEquals(7, count($date['week']));
//        $this->assertEquals(1, count($date['yesterday']));
//    }
//
//
//    public function testコンフィグの設定()
//    {
//        $this->Redis->setConfig(
//            array(
//                'namespace' => 'testConfig',
//
//            )
//        );
//
//
//        $this->assertEquals('testConfig', $this->Redis->namespace);
//    }
//
//
//
//    public function testスコアをインクリメント()
//    {
//        $this->Redis->setScore('intarou','0');
//        $this->Redis->increment('intarou',5);
//        $this->assertEquals($this->Redis->getScore('intarou'),5);
//    }
//
//    public function testスコアをデクリメント()
//    {
//        $this->Redis->setScore('detarou','30');
//        $this->Redis->increment('detarou',-16);
//        $this->assertEquals($this->Redis->getScore('detarou'),14);
//    }
//
//
//
//
//    public function test閲覧履歴を作る()
//    {
//        $this->Redis->setConfig(
//            array(
//                'namespace' => 'rireki_25',
//            )
//        );
//
//
//        //重複も自動でなくなる url / timestamp の順で格納
//        //ページID と timestamp で入れればいいのか。
//        $this->Redis->setScore('ringo','10');
//        $this->Redis->setScore('tarou','20');
//        $this->Redis->setScore('ringo','30');
//        $this->Redis->setScore('ringo','50');
//
//        $this->assertEquals(array('ringo' => 50 , 'tarou' => 20), $this->Redis->getRevRange(0, -1, true));
//
//
//
//    }
//
//    public function test履歴を登録()
//    {
//        //ユーザーIDとurlを記録
//        $this->assertEquals(true, $this->Redis->addList('history','http://www.hogeman.com'));
//
//        //delete
//        $this->assertEquals(true, $this->Redis->deleteList('history'));
//        $this->assertEquals('1', $this->Redis->addList('history','http://www.hogeman.com'));
//
//        //追加したものを読み出す
//        $this->assertEquals(true, $this->Redis->deleteList('history'));
//
//        $this->assertEquals(true, $this->Redis->addList('history','http://www.yahoo.co.jp'));
//        $this->assertEquals(true, $this->Redis->addList('history','http://www.hogeman.com'));
//
//        $this->assertEquals(true, $this->Redis->addList('history','http://www.google.co.jp'));
//        $this->assertEquals(true, $this->Redis->addList('history','http://www.manta.com'));
//
//        $this->assertEquals(array('http://www.manta.com','http://www.google.co.jp','http://www.hogeman.com','http://www.yahoo.co.jp'), $this->Redis->getList('history'));
//
//        //最初のひとつは無視する
//        $this->assertEquals(array('http://www.google.co.jp','http://www.hogeman.com','http://www.yahoo.co.jp'), $this->Redis->getList('history',1));
//
//    }
//
//
//
//
//
//
//
//
//
//
//
//    public function testスコアをセット()
//    {
//
//        $this->Redis->delete(10);
//        $this->assertEquals(true, $this->Redis->setScore(10, 500));
//    }
//
//
//
//    public function test指定ユーザーを削除()
//    {
//        $this->assertEquals(true, $this->Redis->delete(10));
//    }
//
//    public function test数値をセット及び取得()
//    {
//        $this->Redis->setScore(10, 500);
//        $this->assertEquals(500, $this->Redis->getScore(10));
//
//        $this->Redis->setScore(13, 1.5);
//        $this->assertEquals(1.5, $this->Redis->getScore(13));
//    }
//
//    public function test日本語もalphabetセット及び取得()
//    {
//        $this->Redis->setScore('日本語もいけるか？', 33);
//        $this->assertEquals(0, $this->Redis->getScore(33));
//
//
//        $this->Redis->delete('tarou');
//        $this->Redis->setScore('tarou', 55);
//        $this->assertEquals(55, $this->Redis->getScore('tarou'));
//
//        $this->Redis->delete('日本語');
//        $this->Redis->setScore('日本語', 55);
//        $this->assertEquals(55, $this->Redis->getScore('日本語'));
//    }
//
//
//    public function test全データを削除()
//    {
//        $this->Redis->setScore('tarou', 55);
//        $this->Redis->setScore('jirou', 25);
//
//        $this->Redis->deleteAll();
//
//        $this->Redis->deleteAll();
//        $this->assertEquals(array(), $this->Redis->deleteAll());
//
//    }
//
//
//    public function test全員の順位を取得()
//    {
//        $this->Redis->deleteAll();
//        $this->Redis->setScore('tarou', 10.5);
//        $this->Redis->setScore('jirou', 80);
//        $this->Redis->setScore('hanako', 1);
//
//        $this->assertEquals(array('jirou' => 80, 'tarou' => 10.5, 'hanako' => 1), $this->Redis->getRevRange(0, -1, true));
//
//    }
//
//
//    public function test自分の順位を取得()
//    {
//        $this->Redis->deleteAll();
//        $this->Redis->setScore('tarou', 10.5);
//        $this->Redis->setScore('jirou', 80);
//        $this->Redis->setScore('hanako', 1);
//
//        $this->assertEquals(1, $this->Redis->getRank('jirou'));
//        $this->assertEquals(3, $this->Redis->getRank('hanako'));
//    }
//
//    public function test参加人数を取得()
//    {
//        $this->Redis->deleteAll();
//        $this->assertEquals(0, $this->Redis->countAll());
//
//        $this->Redis->setScore('tarou', 10.5);
//        $this->Redis->setScore('jirou', 80);
//
//        $this->assertEquals(2, $this->Redis->countAll());
//    }
//
//    public function test指定範囲のスコアを持つユーザーを取得()
//    {
//        $this->Redis->deleteAll();
//        $this->Redis->setScore('10tarou', 100);
//        $this->Redis->setScore('9jirou', 90);
//        $this->Redis->setScore('8hanako', 80);
//        $this->Redis->setScore('7tarou', 70);
//        $this->Redis->setScore('6jirou', 60);
//        $this->Redis->setScore('hideki', 50);
//        $this->Redis->setScore('4tarou', 40);
//        $this->Redis->setScore('3jirou', 30);
//        $this->Redis->setScore('2hanako', 20);
//        $this->Redis->setScore('1tarou', 10);
//
//
//        $this->assertEquals(array('2hanako' => 20, '3jirou' => 30), $this->Redis->getRangeByScores(20, 35, true));
//    }
//
//    public function test自分の順位の3つ上のユーザーを全て求める()
//    {
//
//        $this->Redis->deleteAll();
//        $this->Redis->setScore('10tarou', 100);
//        $this->Redis->setScore('9jirou', 90);
//        $this->Redis->setScore('8hanako', 80);
//        $this->Redis->setScore('7tarou', 70);
//        $this->Redis->setScore('6jirou', 60);
//        $this->Redis->setScore('hideki', 50);
//        $this->Redis->setScore('4tarou', 40);
//        $this->Redis->setScore('3jirou', 30);
//        $this->Redis->setScore('2hanako', 20);
//        $this->Redis->setScore('1tarou', 10);
//
//
//        $this->Redis->setScore(array('6jirou' => 60, '7tarou' => 70, '8hanako' => 80), $this->Redis->getNearRank('hideki'));
//        $this->Redis->setScore(array(), $this->Redis->getNearRank('10tarou'));
//    }


}
