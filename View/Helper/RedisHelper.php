<?php

App::uses('HtmlHelper', 'View/Helper');
App::uses('Set', 'Utility');

//a
class RedisHelper extends HtmlHelper
{
    public $namespace = 'your';
    public $Redis;

    public function __construct(View $View, $settings)
    {
        parent::__construct($View, $settings);
        if (is_object($this->_View->response)) {
            $this->response = $this->_View->response;
        } else {
            $this->response = new CakeResponse();
        }
        if (!empty($settings['configFile'])) {
            $this->loadConfig($settings['configFile']);
        }


        $this->Redis = new Redis();

        $this->Redis->connect('127.0.0.1', 6379);
        $this->Redis->select($settings['select']);

        if(!empty($settings['namespace'])){
            $this->namespace = $settings['namespace'];
            $this->Redis->namespace = $settings['namespace'];
        }

    }

//    新着情報を挿入 user_id と url を入れる
    public function addNews($user_id, $message_url)
    {
        return $this->Redis->zadd($user_id, time(),$message_url);
    }

//新着情報を取得
    public function getNews($user_id,$end = 100)
    {
        return $this->Redis->zRevRange($user_id,0, $end,true);
    }

//    ニュースを削除
    public function deleteNews($user_id,$message_url)
    {
        return $this->Redis->zRem($user_id,$message_url);
    }


    //新着ニュースをカウント カウント範囲として、ダミータイムスタンプを入れる。 1 と　142371258500　
    //　end を入れるとそれ以上の件数があれば　40+ みたいな表記にする
    public function countNews($user_id,$end = 999)
    {
        $ct =  $this->Redis->zCount($user_id, 1, 142371258500);
        if($ct > $end){
            $ct = $end."+";
        }

        return $ct;
    }








    public function getKeys($pattern = "*")
    {
        $res = $this->Redis->keys($pattern);
        return $res;
    }



    public function exists($key)
    {
        $res = $this->Redis->exists($key);
        return $res;
    }

    public function setData($key,$value)
    {
        $res = $this->Redis->set($key,$value);
        return $res;
    }

    public function getData($key)
    {
        $res = $this->Redis->get($key);
        return $res;
    }

    public function revRange($name,$start = 0, $end = -1, $withscores = false)
    {
        return $this->Redis->zRevRange($name, $start, $end, $withscores);
    }

    public function score($name,$user_id)
    {
        return $this->Redis->zScore($name, $user_id);
    }


    public function incr($name,$user_id,$increment = 1)
    {
        return $this->Redis->zIncrBy($name, $increment, $user_id);
    }

















    public function getRangeDate($kikan = 30)
    {
        $date = new DateTime();

        $range = array(
            'today' => $date->format('Y-m-d')
        );
        $i = 0;
        while($i < $kikan){
            $date->modify('-1 days');
            $range['month'][] = $date->format('Y-m-d');

            if($i < 7){
                $range['week'][] = $date->format('Y-m-d');

                if($i < 1){
                    $range['yesterday'][] = $date->format('Y-m-d');
                }
            }

            $i++;
        }

        return $range;

    }

    public function set_namespace($namespace)
    {
        $this->namespace = $namespace;
        $this->Redis->namespace = $namespace;
    }

    public function hSet($name,$key,$value)
    {
        $res = $this->Redis->hSet($name,$key,$value);
        return $res;

    }

    public function hGet($name,$key)
    {
        $res = $this->Redis->hGet($name,$key);
        return $res;

    }

    public function hIncrement($name,$key)
    {
        $res = $this->Redis->hincrby($name,$key,1);
        return $res;
    }




    public function isUnique($url,$remote ="")
    {

        $res = false;
        if(!$this->Redis->hExists($url,$remote)){
            //前のやつの読み込む
            $before = $this->Redis->hKeys($url);
            foreach ($before as $v) {
                $this->Redis->delete($url,$v);
            }

            $this->Redis->hSet($url,$remote,'hoge');
            $res = true;
        }




        return $res;
    }




    public function zSize($key)
    {
        return $this->Redis->zSize($key);
    }














    public function increment($user_id,$increment,$unique = false,$remote = '')
    {



        if($unique == true){
            if($remote == ""){
                $remote = $_SERVER['SERVER_ADDR'];
            }

            if($this->isUnique($user_id,$remote)){
                return $this->Redis->zIncrBy($this->namespace, $increment, $user_id);
            } else {
                return $this->getScore($user_id);
            }
        } else {
            return $this->Redis->zIncrBy($this->namespace, $increment, $user_id);
        }



    }


    public function addList($list_name, $item)
    {
        $tmp = $this->Redis->lRange($list_name,0,-1);
        return $this->Redis->lPush($list_name, $item);
    }

    public function deleteList($list_name)
    {
        return $this->Redis->delete($list_name);
    }

    public function getList($list_name,$start = 0)
    {
        return $this->Redis->lRange($list_name,$start,-1);
    }


    public function setConfig($config = array())
    {
        $this->namespace = $config['namespace'];
    }

    public function setScore($user_id, $score)
    {
        return $this->Redis->zadd($this->namespace, $score, $user_id);
    }

    public function delete($user_id)
    {
        return $this->Redis->zDelete($this->namespace, $user_id);
    }

    public function getScore($user_id)
    {
        return $this->Redis->zScore($this->namespace, $user_id);
    }


    public function getRange($start = 0, $end = -1, $withscores = false)
    {
        return $this->Redis->zRange($this->namespace, $start, $end, $withscores);
    }


    public function deleteAll($namespace = '')
    {
        if($namespace == ''){
            $namespace = $this->namespace;
        }


        $deleted_user_id = array();
        foreach ($this->Redis->zRange($namespace, 0, -1) as $user_id) {
            $this->Redis->zDelete($namespace, $user_id);
            $deleted_user_id[] = $user_id;
        }

        return $deleted_user_id;
    }

    public function getRank($user_id)
    {
        return $this->Redis->zRevRank($this->namespace, $user_id) + 1;
    }

    public function getRevRange($start = 0, $end = -1, $withscores = false)
    {
        return $this->Redis->zRevRange($this->namespace, $start, $end, $withscores);
    }

    public function getRangeByScores($start = 0, $end = -1, $withscores = false, $offset = null, $count = null)
    {
        $options = array();
        $options['withscores'] = $withscores;
        if ($offset && $count) {
            $options['limit'] = array($offset, $count);
        }

        return $this->Redis->zRangeByScore(100, $start, $end, $options);
    }


    public function countAll()
    {
        return $this->Redis->zSize($this->namespace);
    }

    public function getNearRank($user_id)
    {
        $rank = $this->getRank('hideki');
        $this->getRange($rank - 1, $rank + 1, true);
    }



}
