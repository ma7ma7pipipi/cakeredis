<? if (count($res) != 0): ?>

    <? foreach ($res as $v): ?>
        <li data-readid="<?= $v['Notice']['id']; ?>" class="read_flag<?=$v['Notice']['read_flag'];?>">
            <span>
                <a href="<?= $v['Notice']['url']; ?>"><?= $v['Notice']['body']; ?></a>
            </span>
        </li>
    <? endforeach; ?>

    <li class="t-c"><a href="/cakeredis/redis/notice/">もっと見る</a></li>
<? else: ?>
    <li class="t-c">新着情報はありません</li>
<? endif; ?>

