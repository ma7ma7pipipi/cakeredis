<?
App::import('Vendor', 'Cakeredis.Newsredis',array('file' => 'Newsredis' . DS . 'Newsredis.php'));
if (class_exists('Newsredis')) {
    $newsredis = new Newsredis();
}

$notice_count = $newsredis->get(u('id'));
?>


<? if ($notice_count == 0): ?>

    <a id="notice-icon" data-toggle="dropdown" class="dropdown-toggle navi-btn">
        <i class="fa fa-bell"></i><span class="badge"><?=$notice_count;?></span>
    </a>


<? else: ?>

    <a id="notice-icon" data-toggle="dropdown" class="dropdown-toggle notice-on navi-btn">
        <i class="fa fa-bell"></i><span class="badge"><?=$notice_count;?></span>
    </a>



<? endif; ?>





<ul class="dropdown-menu bullet notice pull-right" id="notice-info">

</ul>

