<?php

App::uses('AppController', 'Controller');
require_once(ROOT.'/Plugin/Contact/Config/bootstrap.php');
class RedisController extends AppController {

    public $uses = array("Cakeredis.Notice");


    public function read()
    {
        $this->Notice->noticeRead(u('id'),$this->params->query['id']);
        $this->autoRender = false;
    }

    public function getAjaxNotice()
    {

        $this->paginate = array(
            'Notice' =>
                array(
                    'conditions' => array(
                        array('user_id' => u('id'))
                    ),
                    'limit' => 5,
                    'order' => array('Notice.id' => 'desc'),
                )
        );

        $res = $this->paginate('Notice');

        $this->set(
            compact(
                'res'
            )
        );

        $this->layout = 'ajax';
    }


    public function notice()
    {

        $this->paginate = array(
            'Notice' =>
                array(
                    'conditions' => array(
                        array('user_id' => u('id'))
                    ),
                    'limit' => 15,
                    'order' => array('Notice.id' => 'desc'),
                )
        );

        $res = $this->paginate('Notice');

        $this->set(
            compact(
                'res'
            )
        );

        $this->layout = '1column';
    }



}

