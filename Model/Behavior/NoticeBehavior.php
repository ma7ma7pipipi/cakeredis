<?php

App::uses( 'CakeEmail', 'Network/Email');
class NoticeBehavior extends ModelBehavior {


    /*
     * お知らせを追加し、お知らせを +1
     */
    public function noticeAdd(Model $Model,$user_id,$url,$body)
    {

        $this->Notice = Classregistry::init('Cakeredis.Notice');

        $data = array(
            'user_id' => $user_id,
            'url' => $url,
            'body' => $body
        );

        
        $this->Notice->set($data);
        if ($this->Notice->save($data,false)) {
            App::import('Vendor', 'Cakeredis.Newsredis',array('file' => 'Newsredis' . DS . 'Newsredis.php'));
            if (class_exists('Newsredis')) {
                $newsredis = new Newsredis();
            }

            $count = $this->Notice->find('count', array('conditions' => array('user_id' => $user_id ,'read_flag' => 0)));
            $newsredis->set($user_id,$count);

        }

    }

    /*
     * お知らせ読み込み、すでに読み込み済みのお知らせでない場合、お知らせを -1
     */
    public function noticeRead(Model $Model,$user_id,$notice_id)
    {
        $this->Notice = Classregistry::init('Cakeredis.Notice');
        if($this->Notice->find('first', array('conditions' => array('id' => $notice_id,'read_flag' => 0)))){

            $data = array(
                'id' => $notice_id,
                'read_flag' => 1
            );

            $this->Notice->set($data);
            if ($this->Notice->save($data,false)) {
                App::import('Vendor', 'Cakeredis.Newsredis',array('file' => 'Newsredis' . DS . 'Newsredis.php'));
                if (class_exists('Newsredis')) {
                    $newsredis = new Newsredis();
                }

                $count = $this->Notice->find('count', array('conditions' => array('user_id' => $user_id ,'read_flag' => 0)));
                $newsredis->set($user_id,$count);


            }

        }
        



    }



}