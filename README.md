# README #

## noticeプラグイン使い方 ##


### 新着読み出し ###

ヘッダに追加

```
#!php

    <link rel="stylesheet" type="text/css" href="/Cakeredis/css/cakeredis.css" />
    <script type="text/javascript" src="/Cakeredis/js/cakeredis.js"></script>

```

ベルアイコンを読み出し

```
#!php

<?=
                                $this->element('bell', array(), array('plugin' => 'Cakeredis')); ?>
```



```
#!php

--
-- テーブルの構造 `sweet_notices`
--

CREATE TABLE IF NOT EXISTS `sweet_notices` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `body` text NOT NULL,
  `read_flag` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COMMENT='ユーザーへの新着お知らせ' AUTO_INCREMENT=18 ;
```



新着一覧ページ

/cakeredis/redis/notice/



### 新着書き込み ###


ビヘイビアを読み出し
```
#!php

class AppModel extends Model {

    public $actsAs = array(
        'Cakeredis.Notice'
    );
```


user_id , url , body を渡す
```
#!php
$this->Ninki->noticeAdd(
                        $this->request->data['Host']['host_id'],
                        "/hosts/view/".$this->request->data['Host']['host_id']."/",
                        'おめでとうございます。 '.$this->request->data['Host']['name'].' さまより '.$this->request->data['Host']['ninkido'].' <i class="fa fa-heart fa-fw"></i>頂きました！'
                    );


```





















CakephpからRedisを使うためのヘルパー
テストもオールグリーン済み

### What is this repository for? ###

* RedisHelper.php
* 1.0

### How do I get set up? ###

* プラグインに突っ込む
* ヘルパー読み込む

### Contribution guidelines ###

```php
public $helpers = array(
        'Redis' => array('className' => 'Cakeredis.Redis'),
    );
```

### エラー時の注意 ###
・エラーが出た場合は変数の型がおかしい場合があります。
変数自体を削除しましょう。