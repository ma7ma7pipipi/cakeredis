$ ->
  $('#notice-icon').click ->
    $.ajax
      url: '/cakeredis/redis/getajaxnotice/'
      success: (msg) ->
        $('#notice-info').html msg

  $(document).on 'click', '*[data-readid]', ->
    data = id: $(this).data('readid')
    $.ajax
      url: '/cakeredis/redis/read/'
      data: data
      success: (msg) ->
