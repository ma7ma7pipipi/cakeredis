<?php

class Newsredis {

    public function __construct() {
        $config = Configure::read('Newsredis');
        $this->Redis = new Redis();
        $this->Redis->connect('127.0.0.1', 6379);
        $this->Redis->select($config['select']);
    }

    //値を取得
    public function get($user_id)
    {
        return $this->Redis->get($user_id);
    }

    //    値をセット
    public function set($user_id,$int)
    {
        return $this->Redis->set($user_id,$int);
    }

    //    +1
    public function incr($user_id)
    {
        return $this->Redis->incr($user_id);
    }

    //    -1
    public function decr($user_id)
    {
        return $this->Redis->decr($user_id);
    }


    //    新着情報を挿入 user_id と url を入れる
    public function addNews($user_id, $array)
    {
        $message = implode("|",$array);
        return $this->Redis->zadd($user_id, time(),$message);
    }

    //新着情報を取得
    public function getNews($user_id,$end = 100)
    {
        return $this->Redis->zRevRange($user_id,0, $end,true);
    }

    //    ニュースを削除
    public function deleteNews($user_id,$message_url)
    {
        return $this->Redis->zRem($user_id,$message_url);
    }

    //新着ニュースをカウント カウント範囲として、ダミータイムスタンプを入れる。 1 と　142371258500　
    //　end を入れるとそれ以上の件数があれば　40+ みたいな表記にする
    public function countNews($user_id,$end = 999)
    {
        $ct =  $this->Redis->zCount($user_id, 1, 142371258500);
        if($ct > $end){
            $ct = $end."+";
        }

        return $ct;
    }


}