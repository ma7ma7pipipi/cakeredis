# Newsredis使い方 #


* どこでもRedisを使うことができる
* bootstrapでNewsredisを定義


### bootstrap.php ###


```
#!php

//news redis
Configure::write('Newsredis', array(
        'select'=>'3',//DBno
    )
);

```


### 使い方 ###


```
#!php

App::import('Vendor', 'Newsredis/Newsredis');
$newsredis = new Newsredis();

$newsredis->addNews(100, "パスタ");

$count = $newsredis->countNews(100,50);
pd($count);

$res = $newsredis->getNews(100,50);

$newsredis->deleteNews(100,"パスタ");
$res = $newsredis->getNews(100,50);
pd($res);

```




